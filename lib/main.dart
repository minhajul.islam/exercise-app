import 'package:exerciseapp/Colors.dart';
import 'package:exerciseapp/DetailedScreen.dart';
import 'package:exerciseapp/widgets/Category.dart';
import 'package:exerciseapp/widgets/SearchBar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Exercise App',
      theme: ThemeData(
        fontFamily: 'Cairo',
        scaffoldBackgroundColor: mBackgroundColor,
        textTheme: Theme.of(context).textTheme.apply(displayColor: mTextColor),
      ),
      home: HomeScreen(),
    );
  }
}

class HomeScreen extends StatelessWidget {
  createAlertDialog(BuildContext context) {
    return showDialog(context: context);
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    // TODO: implement build

    return Scaffold(
      bottomNavigationBar: Container(
        height: 50,
        color: Colors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Nav_item(
              mIcon: "icons/calendar.svg",
              mTitile: "Today",
            ),
            Nav_item(
              mIcon: "icons/gym.svg",
              mTitile: "All Exercises",
            ),
            Nav_item(
              mIcon: "icons/Settings.svg",
              mTitile: "Settings",
            ),
          ],
        ),
      ),
      body: Stack(
        children: <Widget>[
          Container(
            height: size.height * 0.45,
            decoration: BoxDecoration(
                color: mHeaderColor,
                image: DecorationImage(
                    alignment: Alignment.centerLeft,
                    image: AssetImage("images/undraw_pilates_gpdb.png"))),
          ),
          SafeArea(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Align(
                    alignment: Alignment.topRight,
                    child: Container(
                      alignment: Alignment.center,
                      height: 42,
                      width: 42,
                      decoration: BoxDecoration(
                        color: mIconMenuColor,
                        shape: BoxShape.circle,
                      ),
                      child: SvgPicture.asset("icons/menu.svg"),
                    ),
                  ),
                  Text(
                    'Good Morning',
                    style: Theme.of(context)
                        .textTheme
                        .display1
                        .copyWith(fontWeight: FontWeight.w600, fontSize: 26.0),
                  ),
                  SearchBar(),
                  Expanded(
                    child: GridView.count(
                      crossAxisCount: 2,
                      childAspectRatio: .85,
                      mainAxisSpacing: 15,
                      crossAxisSpacing: 15,
                      children: <Widget>[
                        Category(
                            mPictureIcon: "icons/Hamburger.svg",
                            mTitleIcon: "Diet Recommendation",
                            press: () {}),
                        Category(
                          mPictureIcon: "icons/Excrecises.svg",
                          mTitleIcon: 'Kegel Exercises',
                          press: () {},
                        ),
                        Category(
                          mPictureIcon: "icons/Meditation.svg",
                          mTitleIcon: 'Meditation',
                          press: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => DetailedScreen()));
                          },
                        ),
                        Category(
                          mPictureIcon: "icons/yoga.svg",
                          mTitleIcon: 'Yoga',
                          press: () {},
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

class Nav_item extends StatelessWidget {
  final String mIcon;
  final String mTitile;

  const Nav_item({
    Key key,
    this.mIcon,
    this.mTitile,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: Column(
        children: <Widget>[
          SvgPicture.asset(mIcon),
          Text(mTitile),
        ],
      ),
    );
  }
}
