import 'package:flutter/material.dart';
import '../Colors.dart';

class SessionCard extends StatelessWidget {
  String sessionNum;
  bool isDone;
  Function press;

  SessionCard({
    Key key,
    this.sessionNum,
    this.isDone,
    this.press,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return Container(
        width: constraints.maxWidth / 2 - 10,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(13.0),
        ),
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: press,
            child: Row(children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Container(
                  height: 42,
                  width: 43,
                  decoration: BoxDecoration(
                      color: isDone ? mBlueColor : Colors.white,
                      shape: BoxShape.circle,
                      border: Border.all(color: mBlueColor, width: 2.0)),
                  child: Icon(
                    Icons.play_arrow,
                    color: isDone ? Colors.white : Colors.black,
                  ),
                ),
              ),
              Text(
                sessionNum,
                style: Theme.of(context).textTheme.subtitle,
              )
            ]),
          ),
        ),
      );
    });
  }
}
