import 'package:exerciseapp/Colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class Category extends StatelessWidget {
  final String mPictureIcon;
  final String mTitleIcon;
  final Function press;

  Category({
    Key key,
    this.mPictureIcon,
    this.mTitleIcon,
    this.press,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(15.0),
          boxShadow: [
            BoxShadow(
              offset: Offset(0, 17),
              blurRadius: 17,
              spreadRadius: -23,
              color: mShadowColor,
            )
          ]),
      padding: EdgeInsets.all(20.0),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          onTap: press,
          child: Column(
            children: <Widget>[
              SvgPicture.asset(mPictureIcon),
              Spacer(),
              Text(
                mTitleIcon,
                style: Theme.of(context).textTheme.title.copyWith(
                      fontSize: 15.0,
                    ),
                textAlign: TextAlign.center,
              ),
              Spacer(),
            ],
          ),
        ),
      ),
    );
  }
}
