import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class NavigationBar extends StatelessWidget {
  const NavigationBar({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Nav_item(
            mIcon: "icons/calendar.svg",
            mTitle: "Today",
          ),
          Nav_item(
            mIcon: "icons/gym.svg",
            mTitle: "All Exercises",
          ),
          Nav_item(
            mIcon: "icons/Settings.svg",
            mTitle: "Settings",
          ),
        ],
      ),
    );
  }
}

class Nav_item extends StatelessWidget {
  final String mIcon;
  final String mTitle;

  const Nav_item({
    Key key,
    this.mIcon,
    this.mTitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(30.0),
      child: Column(
        children: <Widget>[
          SvgPicture.asset(mIcon),
          Text(mTitle),
        ],
      ),
    );
  }
}
