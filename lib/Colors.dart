import 'package:flutter/material.dart';

const mBackgroundColor = Color(0xFFF8F8F8);
const mActiveIconColor = Color(0xFFE68342);
const mTextColor = Color(0xFF222B45);
const mBlueLightColor = Color(0xFFC7B8F5);
const mBlueColor = Color(0xFF817DC0);
const mShadowColor = Color(0xFFE6E6E6);
const mHeaderColor = Color(0xFFF5CEB8);
const mIconMenuColor = Color(0xFFF2BEA1);
