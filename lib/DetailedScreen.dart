import 'package:exerciseapp/widgets/NavigationBar.dart';
import 'package:exerciseapp/widgets/SearchBar.dart';
import 'package:exerciseapp/widgets/SessionCard.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'Colors.dart';

class DetailedScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    return Scaffold(
      bottomNavigationBar: NavigationBar(),
      body: Stack(
        children: <Widget>[
          Container(
            height: size.height * .45,
            decoration: BoxDecoration(
              color: mBlueLightColor,
              image: DecorationImage(
                image: AssetImage("images/meditation_bg.png"),
              ),
            ),
          ),
          SafeArea(
              child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: size.height * 0.05,
                ),
                Text(
                  "Meditaion",
                  style: Theme.of(context)
                      .textTheme
                      .display1
                      .copyWith(fontWeight: FontWeight.w700),
                ),
                Text(
                  "3-10 MIN Course",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                SizedBox(
                  width: size.width * .7,
                  child: Text(
                      "Live happier and healthier by learning fundamentals of meditaion and mindfulness"),
                ),
                SizedBox(width: size.width * .5, child: SearchBar()),
                Wrap(
                  spacing: 15.0,
                  runSpacing: 15,
                  children: <Widget>[
                    SessionCard(
                      sessionNum: "Session 1",
                      isDone: true,
                      press: () {},
                    ),
                    SessionCard(
                      sessionNum: "Session 2",
                      isDone: false,
                      press: () {},
                    ),
                    SessionCard(
                      sessionNum: "Session 3",
                      isDone: false,
                      press: () {},
                    ),
                    SessionCard(
                      sessionNum: "Session 4",
                      isDone: false,
                      press: () {},
                    ),
                    SessionCard(
                      sessionNum: "Session 5",
                      isDone: false,
                      press: () {},
                    ),
                    SessionCard(
                      sessionNum: "Session 6",
                      isDone: false,
                      press: () {},
                    ),
                  ],
                ),
                SizedBox(height: 10.0),
                Text(
                  'Meditaion',
                  style: Theme.of(context)
                      .textTheme
                      .title
                      .copyWith(fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 10.0),
                Container(
                  padding: EdgeInsets.all(4.0),
                  height: 60,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(13.0),
                      boxShadow: [
                        BoxShadow(
                            offset: Offset(0.0, 17),
                            blurRadius: 23,
                            spreadRadius: -17,
                            color: mShadowColor)
                      ]),
                  child: Row(
                    children: <Widget>[
                      SvgPicture.asset("icons/Meditation_women_small.svg"),
                      Expanded(
                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'Basic 2',
                            style: Theme.of(context).textTheme.subtitle,
                          ),
                          Text('Start your deepen you practice'),
                        ],
                      )),
                      Padding(
                        padding: EdgeInsets.all(5.0),
                        child: SvgPicture.asset("icons/Lock.svg"),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ))
        ],
      ),
    );
  }
}
